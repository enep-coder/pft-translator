# coding: utf-8

from pfttokens import *

class Lexer:
    def __init__(self,Src=''):
        self.Input = Src
        self.Tokens=[]
        self.OptChars = '\\()"\'{}[]\0'
        self.OptInx = [tokenType.BACKSLASH,tokenType.LPAREN,tokenType.RPAREN,
                       tokenType.COMMA,tokenType.DQUOTES,tokenType.QUOTES,
                       tokenType.LCBRACE,tokenType.RCBRACE,tokenType.LSBRACKET,
                       tokenType.RSBRACKET,tokenType.EOF,tokenType.TEXT]
        self.Pos = 0
        self.countTokens = 0

    def peek(self,rPos):
        _pos =  self.Pos + rPos
        if _pos >= len(self.Input):
            return '\0'
        return self.Input[_pos]

    def __str__(self):
        buf = ''
        for i in range(self.countTokens):
            buf += self.Tokens[i].strOpt()+'\n'
        return buf
    def isOptChar(self,char):
        if self.OptChars.find(char):
            return False
        return True

    def getTypeOpt(self,symbol):
        inx = self.OptChars.find(symbol)
        if inx != -1:
            return self.OptChars[inx]
        return len(self.OptChars)

    def step(self):
        self.Pos += 1
        return self.peek()

    def tokenizdeOperator(self):
        char = self.peek()
        self.addToken(self.getTypeOpt(char),char)
        self.step()

    def tokenizeText(self):
        buf = ''
        char = self.peek()
        while self.isOptChar(char) == False:
            buf += char
            char = self.step()
            if char == '\0':
                break
        self.addToken(tokenType.TEXT,buf)

    def tokenize(self):
        while self.Pos < len(self.Input):
            current = self.peek()
            if self.isOptChar(current):
                self.tokenizdeOperator()
            else:
                self.tokenizeText()
        self.addToken(tokenType.EOF)
        return 0

    def addToken(self, tokentype=0, text=''):
        self.Tokens.append(tokenNode(tokentype,text))
        self.countTokens += 1


