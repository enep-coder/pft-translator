PFT Translator
=========================

***
Author : Igor ENEPUNIXOID Kolonchenko

Date: Февраль 2022

Site: [enep-home.ru](https://enep-home.ru)

***

## Introduction

Translator of the **PFT** (Print form template) file into a format for printing.

This format is designed so that templates can be formatted with code as is done in LaTeX

## Format file PFT
### Description
Document template description format for preparing for printing and converting to another format

### AST
```
PFT                   --> BLOCKS EOF
BLOCKS                --> BLOCK {BLOCK}
BLOCK                 --> BACKSLASH DIRECTIVE | TEXT
DIRECTIVE             --> UNARY_DIRECTIVE | PARAM_DIRECTIVE |
                          PARAM_DIRECTIVE_BLOCK | DIRECTIVE_BLOCK
UNARY_DIRECTIVE       --> WORD
PARAM_DIRECTIVE       --> WORD LPAREN PARAMS RPAREN
PARAM_DIRECTIVE_BLOCK --> PARAM_DIRECTIVE LBRACE BLOCKS RBRACE
DIRECTIVE_BLOCK       --> WORD LBRACE BLOCKS RBRACE
WORD                  --> ALPHABET {ALPHABET}
PARAMS                --> PARAM {COMMA PARAM}
PARAM                 -->  VARITABLE | NUMBER | STRING
VARITABLE             --> ALPHABET { ALPHABET | NUMBER}
TEXT                  --> DQUOTE CHARS DQUOTE
STRING                --> DQUOTE STR DQUOTE
CHARS                 --> CHAR {CHAR}
NUMBER                --> DIGIT {DIGIT}
ALPHABET              --> 'a'..'z'|'A'...'Z'
DIGIT                 --> '0'..'9'
BACKSLASH             --> '\'
LPAREN                --> '('
RPAREN                --> ')'
COMMA                 --> ','
DQUOTE                --> '"'
LBRACE                --> '{'
RBRICE                --> '}'
EOF                   --> #0
```
### Test source code of the example PFT file
```TeX
\document(A4,300,Alpum)
\margin(4,4,4,4)
\font('Arial',12)
\begin(document) {
   \textbf{Test} Test 
}
```
