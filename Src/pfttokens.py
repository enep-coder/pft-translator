# coding: utf-8

class tokenType:
    names = ['BACKSLASH','LPAREN','RPAREN','COMMA','DQUOTES','QUOTES',
            'LCBRACE','RCBRACE','LSBRACKET','RSBRACKET','TEXT','EOF']
    BACKSLASH = 0
    LPAREN = 1
    RPAREN = 2
    COMMA = 3
    DQUOTES = 4
    QUOTES = 5
    LCBRACE = 6
    RCBRACE = 7
    LSBRACKET = 8
    RSBRACKET = 9
    TEXT = 10
    EOF = 11

class tokenNode:
    def __init__(self,tokentype,text=''):
        self.Text= text
        self.Type= tokentype
        self.Lenght = len(text)
    def strOpt(self):
        buf = tokenType.names[self.Type]
        buf += '=>['+self.Text+']'
        return buf

